#include "stdafx.h"
#include "Manager.h"
#include "Factory.h"

using namespace std;

Manager::Manager()
{
	mas = NULL;
	max = 0;
}


Manager::~Manager()
{
}

string Manager::EntryNameFile() {
	char str[N] = "";
	cout << "Entry name file: ";
	cin >> str;
	return str;
}

bool Manager::Save() {
	string NameFile = EntryNameFile();
	ofstream file(NameFile);
	if (!file.is_open()) return false;
	if (mas == NULL || max == 0) return false;

	file << max << "\n";

	for (int i = 0; i < max; ++i) {
		mas[i]->save(file);
	}

	file.close();
	return true;
}

void Manager::resize(int a) {
	MilitaryEquipment**temp = NULL;
	int i = 0;
	if ((max == 0) && (mas == NULL)) {
		mas = new MilitaryEquipment*[a];
		max = a;
		return;
	}
	if (a == max)
		return;
	if (a > max) {
		temp = new MilitaryEquipment*[max];
		for (i = 0; i < max; ++i) {
			temp[i] = mas[i];
		}
		delete mas;
		mas = new MilitaryEquipment*[a];
		for (i = 0; i < max; ++i) {
			mas[i] = temp[i];
		}
		delete temp;
		max = a;
	}
	else {
		temp = new MilitaryEquipment*[max];
		for (i = 0; i < max; ++i) {
			temp[i] = mas[i];
		}
		delete mas;
		mas = new MilitaryEquipment*[a];
		for (i = 0; i < a; ++i) {
			mas[i] = temp[i];
		}
		for (; i < max; ++i) {
			delete temp[i];
		}
		delete temp;
		max = a;
	}
	return;
}

void Manager::recreate() {
	if (mas != NULL && max != 0) {
		int i = 0;
		for (i = 0; i < max; ++i)
			delete mas[i];
		mas = NULL;
		max = 0;
	}
}

void Manager::menu_add() {
	int i = 0;
	int c = 0;
	MilitaryEquipment* temp;

	while (1)
	{
		system("cls");
		for (i = 0; i < MAX; ++i) {
			temp = Equipment_created_factory[i]->created();
			std::cout << i << ".";
			temp->Show_name();
			std::cout << std::endl;
			delete temp;
		}
		std::cout << std::endl << MAX << ".Exit\n\n";

		std::cin >> c;
		if (c > MAX) {
			continue;
		}
		if (c == MAX) {
			break;
		}
		resize(max + 1);
		mas[max - 1] = Equipment_created_factory[c]->created();
		mas[max - 1]->filling();
	}
}

void Manager::main_menu() {

	int c = 0;

	while (1) {
		system("cls");
		cout << "1.Show \n2.Add \n3.Delete all\n4.Save in file \n0.Exit\n";
		cin >> c;

		switch (c) {
		case 0:
			return;
		case 1:
			if (max <= 0)
			{
				cout << "No Equipment\n";
				break;
			}
			for (int i = 0; i < max; ++i)
			{
				std::cout << i + 1 << std::endl;
				mas[i]->Show();
			}
			_getch();
			break;
		case 2:
			menu_add();
			break;
		case 3:
			recreate();
			break;
		case 4:
			if (Save())
				cout << "Save complite\n";
			else
				cout << "File Erroe\n";
			_getch();
			break;
		}

		Sleep(1000);
		c = 0;
	}

	return;
}
