#pragma once

#include "USA.h"
#include "Russia.h"
#include "MilitaryEquipment.h"

#define MAX ( sizeof( Equipment_created_factory ) / 4 ) // ����� ������������ ����������

class Factory {
public:
	virtual MilitaryEquipment* created() = 0;
};

template <class T>
class Factory_doc : public Factory {
public:
	MilitaryEquipment * created() { return (new T); }
};

Factory *Equipment_created_factory[] =
{
	new Factory_doc<USA>,
	new Factory_doc<Russia>
};


